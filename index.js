const bodyParser = require('body-parser');
const cors = require('cors');
const express = require( 'express');
const session = require('express-session');
const passport = require('passport');
const passportLocal = require('passport-local');

const model = require('./model');
var ObjectID = require('mongodb').ObjectID;

//mongoose.set('useFindAndModify', false);
const app = express();
const port = process.env.PORT || 3000;


//MIDDLEWARE
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ credentials: true, origin:'null,https://damp-plateau-78510.herokuapp.com'}));
app.use(express.static('client'));
//secret is a sign for cookies
app.use(session( { secret: 'uivpqreuqr3jngoqui3r[w', resave: false, saveUninitialized: true}));
app.use(passport.initialize());
app.use(passport.session());

//PASSPORT CONFIG
passport.use(new passportLocal.Strategy({ 
    //configs
    usernameField: 'email',
    passwordField: 'plainPassword'
}, function( email, plainPassword, done ) { 
    //authentication logic success or fail
    //async find user in DB by email
    model.User.findOne( { email: email}).then(function (user) {
        if (!user) {
            //user not found return failure
            return done(null,false);
        } else {
            //user exists async compare given password to password in DB
            user.verifyPassword(plainPassword, function (result) {
                if (result) {
                    //success
                    return done(null,user);
                } else {
                    //failure
                    return done(null,false);
                }
            });
        }
    }).catch( function (err) {
        //unlikely but database err
        done(err);
    });
}));

// PASSPORT SERIALIZATION AND DESERILIZATION
passport.serializeUser(function (user,done) {
    //called on succesful authentication
    done(null, user._id);
});

passport.deserializeUser( function (userID, done) {
    //called before future requests following succesful authentication
    model.User.findOne( { _id: ObjectID(userID)}).then(function (user) {
        done(null,user);
    }).catch(function (err) {
        done(err);
    });
});
app.delete('/session', function (req,res) {
    req.logOut();
    res.sendStatus(200);
});
app.get('/session', function (req,res) {
    if (req.user) {
        res.json(req.user);
    } else {
        res.sendStatus(401);
    }
});
// RESTful authenticate
app.post('/session', passport.authenticate('local'), function (req, res) {
    res.sendStatus(200);
    //has implied 401 in case we get a fail
});

app.get('/users', function (req,res) {
    model.User.find().then(function (user) {
        res.json(user);
    });
});

app.get('/animals', function (req, res) {


     model.Animal.find().then(function (animals) {
        res.json(animals);
    });
});

app.put('/animals/:id', function (req, res) {
    console.log("body: ", req.body);
    let id = req.params.id
    console.log("PUT var id: ",id );
    console.log("PUT body ID: ", req.body.id)
    var query = { _id: ObjectID(req.body.id) };
    var newval  =  { scheduled: req.body.scheduled  };
    var newvals = { name: req.body.name,
                    breed: req.body.breed,
                    age: req.body.age,
                    gender: req.body.gender,
                    color: req.body.color,
                    scheduled: req.body.scheduled,
                    description: req.body.description};
    console.log(query);
    console.log(newval);
    console.log("BOOL: ", req.body.scheduled);
    //animal being reserved
    if (req.body.scheduled == true) {
        model.Animal.findOneAndUpdate( query, newvals).then(function () {
            res.sendStatus(200);
        });
    }
    //update animal to be put back up for adoption
    model.Animal.findOneAndUpdate( query, newvals).then(function () {
        res.sendStatus(200);
    });


});
app.delete('/animals/:id' , function (req, res) {
    let animalId = req.body.id;
    console.log("DELETE ID: ", animalId);

    model.Animal.findOneAndDelete( {_id: ObjectID(req.body.id) }).then(function (animal) {
        if (animal) {
            res.json(animal);
            
        } else {
            res.sendStatus(404);
        }
    }).catch(function () {
        res.sendStatus(400);
    });
});
app.post('/animals', function (req, res) {
    console.log("body: ", req.body);
    //create animal instance
    let animal = new model.Animal ({
        animal: req.body.animal,
        name: req.body.name,
        breed: req.body.breed,
        age: req.body.age,
        gender: req.body.gender,
        color: req.body.color,
        description: req.body.description,
        scheduled: req.body.scheduled

    });
    console.log("name", animal.name)
    //insert into mongoose model
    animal.save().then(function () {
        res.sendStatus(201);
    }).catch(function (err) {
        if (err.errors) {
          var messages = {};
          for (let e in err.errors) {
            messages[e] = err.errors[e].message;
          }
          res.status(422).json(messages);
        } else {
          res.sendStatus(500);
        }
    });
});

app.post('/users' , function (req,res) {
    
    let user = new model.User({ 
        fName: req.body.fName,
        lName: req.body.lName,
        email: req.body.email
    });
    console.log("BODY",req.body);

    user.setEncryptedPassword(req.body.plainPassword, function () {
        //insert into mongoose model
        user.save().then(function() {
            res.sendStatus(201);
        }).catch(function (err) {
            if (err.errors) {
              var messages = {};
              for (let e in err.errors) {
                messages[e] = err.errors[e].message;
              }
              res.status(422).json(messages);
            } else {
                //duplicate emails 
              res.sendStatus(500);
            }
        });
    });
});

app.listen(port, function () {
    console.log(`Listening on port  ${port}!`)
});
